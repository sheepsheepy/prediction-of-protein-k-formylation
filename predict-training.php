<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/andreas01.css" media="screen" title="andreas01 (screen)" />
<link rel="stylesheet" type="text/css" href="css/print.css" media="print" />
<link href="images/SuccSite.ico" rel="SHORTCUT ICON">
	<?php include("pages/titleicon.html"); ?>
	</head>

<body>
	<div id="wrap">

		<?php
			include("pages/top.html");
			include("pages/left.php");
			include("pages/right.php");
		?>

<table width="805" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF"><tr><td height="164" bgcolor="#FFFFFF">
	<table width="805" height="544" border="0" cellpadding="15"><tr><td height="406" align="left" valign="top" bgcolor="#FFFFFF"><h2>Result</h2>
	<hr />
<?php
	date_default_timezone_set("Asia/Taipei");
	$today = date("Ymd");      // 20040310
	$time = date("His");     // 17:16:17
	$day_time = $today.$time;
	if(!is_dir("temp/".$today))	
		mkdir("temp/".$today,0777);
	$fn = $today."/".$time;


	    	if(copy("predict-training100.fasta","temp/$fn.fasta"))
    		{
        		$fi = fopen("temp/$fn.fasta","r");
        		while($temp = trim(fgets($fi,10000)))
        		{
            			if(substr($temp,0,1) == '>') $name = trim(substr($temp,1));  // ----get the input name 
            			else $seq = trim($temp);             // ----get the input sequence
        		}
    		}
    		else
    		{
        		echo "<div align=\"center\"><span class=\"style1 style15\"><font color=\"red\" face=\"Courier New, Courier, mono\" size=\"4\"><b>You Must Input Sequence or File.</font></span></div>";
        		die;
    		}
	
//----------------Fasta Change Seq ID \t seq------------------
	$fi = fopen("temp/$fn.fasta","r");
	$fs = fopen("temp/$fn.seq","w");
	$i = 0;
	while($line = fgets($fi))
	{
		$line = trim($line);
		if(strpos($line,">") !== false)
		{
			if($i != 0)
			{
				fwrite($fs,"\n");
			}
			$ID = trim(substr($line,1));
			fwrite($fs,$ID."\t");
		}
		else
		{
			fwrite($fs,$line);
		}
		$i++;
	}
	fwrite($fs,"\n");
	fclose($fi);
	fclose($fs);
//------------------Cut the Fragment-----------------------------	
	$fi = fopen("temp/$fn.seq","r");
	$ffk = fopen("temp/$fn.frag","w");
	
	while($line=fgets($fi))
	{
		$ID  = trim(strtok($line,"\t"));	
		$seq = trim(strtok("\t"));
		$seq_len = strlen($seq);
		for($i=0; $i<$seq_len; $i++)
		{
			if($seq[$i]=="K")
			{
				$position = $i + 1;
				fwrite($ffk,$ID."\t".$position."\t");
				//for($j=$i-15; $j<$i; $j++)
				for($j=$i-10; $j<$i; $j++)
				{
					if($j<0){
						fwrite($ffk,"-");	
					}
					else{
						fwrite($ffk,$seq[$j]);
					}
				}
				//for($j=$i; $j<$i+16; $j++)
				for($j=$i; $j<$i+11; $j++)
				{
					if($j >= $seq_len){
						fwrite($ffk,"-");
					}
					else{
						fwrite($ffk,$seq[$j]);	
					}
				}
				fwrite($ffk,"\n");
			}
		}
	}
	fclose($fi);
	fclose($ffk);

//------------------------------------------------------------
	$Threshold = $_POST["Threshold"];
	if($Threshold == "High")
	{
		$SP=0.45;
		//$SP=0.95;
	}
	elseif($Threshold == "Medium")
	{
		$SP=0.6;
		//$SP=0.90;
	}
	elseif($Threshold == "Low")
	{
		$SP=1;
		//$SP=0.85;
	}

	system("php AAC.php ./temp/$fn.frag");

	exec("./QF/PredQuickrbf ./K.libsvm ./K.libsvm.model ./temp/$fn.frag.aac.libsvm ./temp/$fn.out");
	//exec("./QF/PredQuickrbf ./QF/K.libsvm ./QF/K.libsvm.model ./temp/$fn.frag.aac.libsvm ./temp/$fn.out");
	exec("./QF/Weight ./temp/$fn.out \($SP,1\) ./temp/$fn.wt");
	//exec("./QF/Weight ./temp/$fn.out \($SP,0.85\) ./temp/$fn.wt");

// Print 20171224
$fp_ac = fopen("temp/$fn.frag.ac.jp","r");
$count_ans = 0;

while(!feof($fp_ac))
{
        $tmp = fgets($fp_ac);
        $count_ans++;
}

rewind($fp_ac);

//$fp_div_in = fopen("temp/$fn.frag.ac.jp", "r");

for($i = 0; $i < $count_ans - 1; $i++)
{
        $val = fgets($fp_ac);
        $fp_div = fopen("temp/$fn"."$i.ac.jp", "w");
        fwrite($fp_div,$val);
        fclose($fp_div);
}

for($i=0;$i<$count_ans-1;$i++)
{
        system("php ./bartutex1.php temp/$fn$i.ac.jp > temp/$fn$i.png");
}
fclose($fp_ac);
//fclose($fp_div_in);
//



	exec("php finish.php ./temp/$fn.frag ./temp/$fn.wt ./temp/$fn.seq");
 	
	$fi = fopen("./temp/".$fn.".finish","r");

/*
        for($p=1;$p<=14;$p++)
        {
        	$mddlogo[$p] =  "<a href=\"./MDD_motif/CS".$p.".png\" target=\"blank\"><img src=\"./MDD_motif/CS".$p.".png\" width=\"210\"></a>";
        }
*/
	$count2 = 0;
	$pos_num = 0;
	while(!feof($fi))
	{
		$temp = fgets($fi);
		$guess[$count2]=strtok($temp,"\t");
		$id[$count2]=strtok("\t");
		$pos[$count2]=strtok("\t");
		$fr[$count2]=strtok("\t");
		strtok("\t");
		$sequence[$count2]=strtok("\t");
		$GuessValue[$count2]=strtok("\t");
		//$PWMvalue[$count2]=strtok("\t");
		
		if($guess[$count2]==1)
			$pos_num++;
		
		$count2++;
			
	}
	fclose($fi);
	
	if($pos_num == "0")	
	{
		echo "<div align=\"center\">";
		echo "<font color=\"red\" face=\"Courier New, Courier, mono\" size=\"4\"><b>";
		echo " No Succinylation site has been predicted!";
		echo "</b></font></div></td>";
	}
	else
	{
		for($x=0;$x<$count2-1;$x++)
		{
			if($id[$x]!=$id[$x-1])
			{
				echo "<br><table width=\"700\" border=\"1\" cellspacing=\"3\" cellpadding=\"3\" bordercolordark=\"#ffffff\" bordercolorlight=\"#929292\" align=\"center\">";
				echo "<tr>";
					echo "<td colspan=\"2\" height=\"18\" background=\"images/header_middle.gif\" align=\"center\"><font color=\"#FFFFFF\" face=\"Arial, Helvetica, sans-serif\" size=\"3\"><a href='./temp/$fn.finish'><b>Download Result</b></a></font></td>";
					echo "<td colspan=\"3\" height=\"18\" background=\"images/bg_headline_td_1.gif\" align=\"center\"><font color=\"#FFFFFF\" face=\"Arial, Helvetica, sans-serif\" size=\"3\"><b>Input Information</b></font></td>";
				echo "</tr>";

	        		echo "<tr>";
					echo "<td colspan=\"2\" bgcolor=\"#F1F1F1\" align=\"center\" class=\"style11\"><font color=\"#003399\"><b>Specificity level</b></font></td>";
					echo "<td colspan=\"3\" bgcolor=\"#FFFFFF\" align=\"center\"><font color=\"#000000\" face=\"Courier New, Courier, mono\" size=\"2\">".$Threshold."</font></td>";
				echo "</tr>";

				echo "<tr>";
					echo "<td colspan=\"2\" bgcolor=\"#F1F1F1\" align=\"center\" class=\"style11\"><font color=\"#003399\"><b>Input ID</b></font></td>";
					echo "<td colspan=\"3\" bgcolor=\"#FFFFFF\" align=\"center\"><font color=\"#000000\" face=\"Courier New, Courier, mono\" size=\"2\">".$id[$x]."</font></td>";
				echo "</tr>";

        			echo "<tr>";
					echo "<td colspan=\"2\" bgcolor=\"#F1F1F1\" align=\"center\" class=\"style11\"><font color=\"#003399\"><b>Input Sequence</b></font></td>";
					echo "<td colspan=\"3\" bgcolor=\"#FFFFFF\" align=\"left\"><font color=\"#000000\" face=\"Courier New, Courier, mono\" size=\"2\">";
					echo "".substr($sequence[$x],0,50)."... <a href=search.php?ID=".$id[$x]."&sequence=".$sequence[$x]." target='_blank'><img src='images/go1.jpg'></a></font></td>";
				echo "</tr>";
			
				echo "<tr>";
					echo "<td colspan=\"5\" height=\"18\" background=\"images/bg_headline_td_1.gif\" align=\"center\"><font color=\"#FFFFFF\" face=\"Arial, Helvetica, sans-serif\" size=\"3\"><b>Predict Result</b></font></td>";
				echo "</tr>";
	
				echo "<tr bgcolor=\"#c1d0df\">";
					echo "<td width=\"\150\" align=\"center\" class=\"style11\"><font color=\"#666666\"><b>Protein Name</b></font></td>";
					echo "<td width=\"70\" align=\"center\" class=\"style11\"><font color=\"#666666\"><b>Locations</b></font></td>";
					echo "<td width=\"210\" align=\"center\" class=\"style11\"><font color=\"#666666\"><b>Succinylation Sites</b></font></td>";
					echo "<td width=\"210\" align=\"center\" class=\"style11\"><font color=\"#666666\"><b>Amino Acid Composition</b></font></td>";
				echo "</tr>";
					//echo "<td width=\"60\" align=\"center\" class=\"style11\"><font color=\"#666666\"><b>Score</b></font></td>";
			}
			else
			{

				if($guess[$x]==1)
                   		{
				
                                        echo "<tr bgcolor=\"#F1F1F1\">";
                                        	echo "<td align=\"center\" class=\"style11\"><font color=\"#000000\" face=\"Courier New, Courier, mono\" size=\"2\">".$id[$x]."</font></td>";
                                        	echo "<td align=\"center\" class=\"style11\"><font color=\"#000000\" face=\"Courier New, Courier, mono\" size=\"2\">".$pos[$x]."</font></td>";

						//echo "<td align=\"center\" class=\"style11\"><font color=\"#000000\" face=\"Courier New, Courier, mono\" size=\"2\">".$fr[$x][0].$fr[$x][1].$fr[$x][2].$fr[$x][3].$fr[$x][4].$fr[$x][5].$fr[$x][6].$fr[$x][7].$fr[$x][8].$fr[$x][9].$fr[$x][10].$fr[$x][11].$fr[$x][12].$fr[$x][13].$fr[$x][14].'<font color=red>&nbsp;&nbsp;<b>'.$fr[$x][15].'</b>&nbsp;&nbsp;</font>'.$fr[$x][16].$fr[$x][17].$fr[$x][18].$fr[$x][19].$fr[$x][20].$fr[$x][21].$fr[$x][22].$fr[$x][23].$fr[$x][24].$fr[$x][25].$fr[$x][26].$fr[$x][27].$fr[$x][28].$fr[$x][29].$fr[$x][30]."</font></td>";
						echo "<td align=\"center\" class=\"style11\"><font color=\"#000000\" face=\"Courier New, Courier, mono\" size=\"2\">".$fr[$x][0].$fr[$x][1].$fr[$x][2].$fr[$x][3].$fr[$x][4].$fr[$x][5].$fr[$x][6].$fr[$x][7].$fr[$x][8].$fr[$x][9].'<font color=red>&nbsp;&nbsp;<b>'.$fr[$x][10].'</b>&nbsp;&nbsp;</font>'.$fr[$x][11].$fr[$x][12].$fr[$x][13].$fr[$x][14].$fr[$x][15].$fr[$x][16].$fr[$x][17].$fr[$x][18].$fr[$x][19].$fr[$x][20]."</font></td>";
						
						echo "<td><div align=\"center\"><a href=\"temp/$fn$x.png\" target=\"blank\"><img src=\"temp/$fn$x.png\" width=\"210\"></a></div></td>";
					echo "</tr>";
					
				}		
				
			}

				
			

			echo "</tr>";
		   }
		echo "</tr></table>";
	}
?>
	</td></tr></table>

<tr><td border="0" colspan="4" align="center"><a href="#top">- top -</a></td></tr><br><br>
</td></tr></table>
<p>&nbsp;</p>



<?php include("pages/buttom.html"); ?>>

</div>
</body>
</html>

