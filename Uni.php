<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/andreas01.css" media="screen" title="andreas01 (screen)" />
<link rel="stylesheet" type="text/css" href="css/print.css" media="print" />
<?php

include("pages/titleicon.html");
?>
</head>

<body><div id="wrap">

<?php
//---------------------top.html--------------------------//
include("pages/top.html");
include("pages/left.php");
include("pages/right.php");
?>

<?php
$QuickValue = $_POST['QuickValue'];
if($QuickValue != "")
{
	system("wget -O ./temp/$QuickValue.fasta http://www.uniprot.org/uniprot/$QuickValue.fasta");
	$open_fasta = fopen("./temp/$QuickValue.fasta","r");
	
	$line = fgets($open_fasta);
	$line = trim($line);
	if($line != "")
	{
		while($line = fgets($open_fasta))
		{
			$line = trim($line);
			$sequence .= $line;
		}
	}
	else
	{
		echo "<table width=\"805\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#FFFFFF\">";
		echo "<tr><td height=\"164\" bgcolor=\"#FFFFFF\"><table width=\"805\" height=\"544\" border=\"0\" cellpadding=\"15\">";
		echo "<tr><td height=\"406\" align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\"><h2>Search error</h2><hr />";
		echo "<div align=\"center\"><b><font color=\"red\" face=\"Courier New, Courier, mono\" size=\"3\">Search error: the input value could not be matched to a UniProtKB ID or AC</font></b>";
		echo "</div></td></tr></table></td></tr></table>";
		system("rm ./temp/$QuickValue.fasta");
		echo die;
	}
}
else
{
	echo "<table width=\"805\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#FFFFFF\">";
	echo "<tr><td height=\"164\" bgcolor=\"#FFFFFF\"><table width=\"805\" height=\"544\" border=\"0\" cellpadding=\"15\">";
	echo "<tr><td height=\"406\" align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\"><h2>Search error</h2><hr />";
	echo "<div align=\"center\"><b><font color=\"red\" face=\"Courier New, Courier, mono\" size=\"3\">Search error: the input value could not be matched to a UniProtKB ID or AC</font></b>";
	echo "</div></td></tr></table></td></tr></table>";
	system("rm ./temp/$QuickValue.fasta");
	echo die;
}
?>

<div id="content">
  <table width="805" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
    <!--DWLayoutTable-->
    <tr>
      <td height="164" bgcolor="#FFFFFF"><table width="805" height="544" border="0" cellpadding="15">
        <tr>
          <td height="406" align="left" valign="top" bgcolor="#FFFFFF"><h2><br>Submission</h2>
             <form action="predict.php" method="post" enctype="multipart/form-data" name="form1" id="form1" onSubmit="check_submit(this)">
              <label>
              	Paste a single sequence or several sequences with <strong>FASTA</strong> format into the field below:
                <textarea name="SEQ" cols="64" rows="5"><?php echo ">".$QuickValue."\n".$sequence; ?></textarea>
                <p><i>Submit a file (&lt; 2MB) in <font color="#FF0000">FASTA</font> format directly from your local disk:</i><br>  		
                  <input name="SEQFILE" size=64 type="file">
                <p>
                  <label>
                   <i>Select your Threshold:</i>      
            		<input name="Threshold" type="radio" value="High" checked="checked" />High
		            <input name="Threshold" type="radio" value="Medium" />Medium
		            <input name="Threshold" type="radio" value="Low" />Low
                  </label>
                </p>
                <input type="submit" value="Submit">
          		<input type="reset" value="Clear fields">
          		<input type="button" value="Example" 
		onclick="window.document.form1.SEQ.value='>THIO_MOUSE\nMVKLIESKEAFQEALAAAGDKLVVVDFSATWCGPCKMIKPFFHSLCDKYSNVVFLEVDVDDCQDVAADCEVKCMPTFQFYKKGQKVGEFSGANKEKLEASITEYA';">
          		</label>
             </form>
            <br>
			<li><a href="case1.php"><font color="#FE2EF7">Case Study 1</font></a> : ES1_MOUSE, ES1 protein homolog, mitochondrial.</li>
			<li><a href="case2.php"><font color="#FE2EF7">Case Study 2</font></a> : GDIR1_MOUSE, Rho GDP-dissociation inhibitor 1.</li>
            </td>
        </tr>
      </table></td>
    </tr>
</table>

<p>&nbsp;</p>

</div>

<?php
include("pages/buttom.html");
system("rm ./temp/$QuickValue.fasta");
?>

</div>
</body>
</html>

