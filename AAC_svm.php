<?php
	$frag=$argv[1]; //baca input dari user yang sudah dibagi windowsize di file predict.php
	$class = "1";

	$aminoacids = array("A","R","N","D","C","Q","E","G","H","I","L","K","M","F","P","S","T","W","Y","V","-");

//-----------For K carbonylation site------------
	$out = fopen("{$frag}.aac.libsvm","w"); //output akhir dari program
	$count= 0;

	$seq_open = fopen($frag,"r");
	
	while($seq_line = fgets($seq_open))
	{
		//$aack = fopen("{$name_k}{$count}.ac.jp","w"); // for Print AAC 2017.12.23 KYH

		$id = strtok($seq_line, "\t");
		if($id == "\n")break;
		$position = strtok("\t");
		$fragment = strtok("\t");
		$fragment = trim($fragment);
		fwrite($out,$class." ");
		foreach($aminoacids as $key => $aa ){
			$toFind = $aa;//amino acid yg akan dicari
			$nilai1 = 0;//set nilai pertama = 0
			$nilai2 = 0;//set nilai kedua = 0
			$pos = strpos($fragment,$toFind);//cari pos
			
			if($pos<10){
				$nilai1 = substr_count($fragment,$toFind);
				//echo $aa."=".$nilai1."<br>";
			}
			elseif($pos>10){
				$nilai2 = substr_count($fragment,$toFind);
				//echo $aa."=".$nilai2."<br>";
			}

			$nilai = ($nilai1 + $nilai2)/20;
			$key += 1;
			fwrite($out,$key.":".$nilai." "); 
			//fwrite($aack,$nilai."\t");		
		}
		fwrite($out,"\n");
		
		$count++;
		//fclose($aack);
	
	}
	
	fclose($seq_open);
	fclose($out);
// end K 

?>
