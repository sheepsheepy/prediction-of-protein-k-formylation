<?php
$input_frag = $argv[1];
$input_guess = $argv[2];
$input_seq = $argv[3];
//$input_jp = $argv[4];

$open_frag = fopen($input_frag,"r");
$open_guess = fopen($input_guess,"r");
$open_seq = fopen($input_seq,"r");

$output = str_replace(".frag",".finish",$input_frag);
$stat_file = fopen($output,"w");

//for($j=0; $j<$count; $j++)
while($fragline = fgets($open_frag))
{
	//$patline = fgets($open_pat);
	$fragline = trim($fragline);
	//echo "$patline<br>";
	$ID = trim(strtok($fragline,"\t"));
	//echo "$ID<br>";
	$position = trim(strtok("\t"));
	$peptide = trim(strtok("\t"));
	$wSize=strlen($peptide);
	
	$guessline = fgets($open_guess);
	$guessline = trim($guessline);
	$guess = strtok($guessline," ");  //$guess=1 or 2(guess class)
	strtok(" ");
	$guess_value = strtok(" ");

	while($seqline = fgets($open_seq))
	{
		$Seq_ID = trim(strtok($seqline,"\t"));
		$Seq = trim(strtok("\t"));
		if($ID==$Seq_ID)
		{
			$sequence = $Seq;
			break;
		}
	}
	
	if($guess == 1)
		fwrite($stat_file,"1\t{$ID}\t{$position}\t{$peptide}\t1\t{$sequence}\t{$guess_value}\n");
	else
		fwrite($stat_file,"2\t{$ID}\t{$position}\t{$peptide}\t1\t{$sequence}\t{$guess_value}\n");
	rewind($open_seq);
}
fclose($open_frag);
fclose($open_guess);
fclose($open_seq);
fclose($stat_file);


?>
