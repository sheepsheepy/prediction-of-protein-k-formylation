<?php
$name=$argv[1];
$input_file=$name.".pat";
$input_matrix = "./QF/GSH_training.matrix";
$class = "1";

$out = fopen($name.".aapc.libsvm","w");
$seq_open = fopen($input_file,"r");
$matrix_open = fopen($input_matrix,"r");
$pair_score = array('0'=>array_fill(0,441,0), '1'=>array_fill(0,441,0));

for($i = 0; $i < 23; $i++)
{
	$matrix_line = fgets($matrix_open);
}
$matrix_line = fgets($matrix_open);
$matrix_line = trim($matrix_line);
$pair_score[0] = explode("\t",$matrix_line);
$matrix_line = fgets($matrix_open);
$matrix_line = trim($matrix_line);
$pair_score[1] = explode("\t",$matrix_line);

while($seq_line = fgets($seq_open))
{
	$count_pair = array_fill(0,441,0);
	$id = strtok($seq_line, "\t");
	if($id == "\n")break;
	$position = strtok("\t");
	$fragment = strtok("\t");
	$fragment = trim($fragment);
	$WSize = strlen($fragment);
	for($i = 1; $i < $WSize; $i++)
	{
		$j = $i - 1;
		$pairs = $fragment[$j].$fragment[$i];
		for($x = 0; $x < 441; $x++)
		{
			if($pair_score[0][$x] == $pairs)
			{
				$count_pair[$x]++;
			}
		}
	}
	//echo $class." ";
	fwrite($out,$class." ");
	for($x = 0; $x < 441; $x++)
	{
		$score = $pair_score[1][$x]*$count_pair[$x];
		if($score == "-0")$score = 0;
		$vector = $x + 1;
		//echo $vector.":".$score." ";
		fwrite($out,$vector.":".$score." ");
		
	}
	//echo "\n";
	fwrite($out,"\n");
}
fclose($seq_open);
fclose($matrix_open);
fclose($out);
?>
