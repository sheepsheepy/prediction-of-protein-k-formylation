﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/andreas01.css" media="screen" title="andreas01 (screen)" />
<link rel="stylesheet" type="text/css" href="css/print.css" media="print" />
<?php
include("pages/titleicon.html");
?>
</head>

<style type="text/css">
</style>

<body><div id="wrap">

<?php
include("pages/top.html");

include("pages/left.php");

include("pages/right.php");
?>

<div id="content">
  <table width="805" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
    <!--DWLayoutTable-->
    <tr>
      <td height="164" bgcolor="#FFFFFF"><table width="805" height="544" border="0" cellpadding="15">
        <tr>
          <td height="406" align="left" valign="top" bgcolor="#FFFFFF"><h2><br>Submission</h2>
             <form action="predict.php" method="post" enctype="multipart/form-data" name="form1" id="form1" onSubmit="check_submit(this)">
				<label>
					Paste a single sequence or several sequences with <strong>FASTA</strong> format into the field below:
					<textarea name="SEQ" cols="64" rows="5"></textarea>
				
				
                <p>
					<i>Submit a file (&lt; 2MB) in <font color="#FF0000">FASTA</font> format directly from your local disk:</i>
					<br>  		
						<input name="SEQFILE" size=64 type="file">
					<br><br>
					<label>
						<i>Select a Specificity Level:</i>      
						<input name="Threshold" type="radio" value="High" checked="checked" />High (95%)
						<input name="Threshold" type="radio" value="Medium" />Medium (90%)
						<input name="Threshold" type="radio" value="Low" />Low (85%)
					</label>
				  
                </p>
					<input type="submit" value="Submit">
					<input type="reset" value="Clear fields">
					</label>
             </form>
            <br>
			<a href="./case.php">
				<h3> >>>>>>>> <font color="#FE2EF7"> Check Training Model & Independent Test</font> <<<<<<<< </h3>
			</a>
            </td>
        </tr>
      </table></td>
    </tr>
</table>

<br>

</div>

<?php
include("pages/buttom.html");
?>

</div>
</body>
</html>

