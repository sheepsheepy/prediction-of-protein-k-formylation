﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/andreas01.css" media="screen" title="andreas01 (screen)" />
<link rel="stylesheet" type="text/css" href="css/print.css" media="print" />
<link href="images/SuccSite.ico" rel="SHORTCUT ICON">
<title>SuccSite</title>
</head>

<style type="text/css">
<!--
.style1 {
	font-size: 15px;
	font-family: Arial, Helvetica, sans-serif;
}
.style11 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	color: #888888;
}
.style24 {color: #444444; font-weight: bold; font-size: 12px; }
.style36 {font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #FFFFFF; font-weight: bold; }
.style37 {
	font-size: 16px;
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-weight: bold;
	color: #222222;
}

-->
</style>

<body><div id="wrap">

<?php
//---------------------top.html--------------------------//
include("top.html");
?>

<div id="avmenu">
<h2 class="hide">Menu:</h2>
<ul>
<li><a href="index.php">Welcome!</a></li>
<li><a href="prediction.php">Predict</a></li>
<li><a href="introduction.php">Documentation</a></li>
<li><a href="download.php">Dataset</a></li>
<li><a href="mailto:francis@saturn.yzu.edu.tw">Contact us</a></li>
<!--<li><a href="tutorial.php">Tutorial</a></li>-->
</ul>

<div class="announce">
<h3>Latest news:</h3>
<p><strong>Dec 14, 2017:</strong><br />
<font color=red><b>SuccSite </b></font>: a web server was constructed for identifying succinylation sites.</p>
<!--<p class="textright"><a href="news.php">Read more...</a></p>-->
</div>

</div>

<div id="extras">
<h3>Useful Link:</h3>
<p>- <a href="http://www.uniprot.org/">UniProtKB<br>&nbsp;&nbsp;(Swiss-Prot)</a><br />
   - <a href="http://dbptm.mbc.nctu.edu.tw/">dbPTM v3</a><br />
   - <a href="http://biocomputer.bio.cuhk.edu.hk/RedoxDB/">RedoxDB</a><br />
   - <a href="http://csb.shu.edu.cn/SGDB/">SGDB</a><br />
   - <a href="http://www.phosphosite.org/">PhosphoSitePlus</a><br />
<p class="small">Version: 1.0<br />
  (Dec 14, 2017)</p>
</div>

<div id="content">
  <table width="805" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
    <!--DWLayoutTable-->
    <tr>
      <td height="164" bgcolor="#FFFFFF"><table width="805" height="544" border="0" cellpadding="15">
        <tr>
          <td height="406" align="left" valign="top" bgcolor="#FFFFFF"><h2><br>Submission</h2>
             <form action="predict.php" method="post" enctype="multipart/form-data" name="form1" id="form1" onSubmit="check_submit(this)">
              <label>
              	Paste a single sequence or several sequences with <strong>FASTA</strong> format into the field below:
                <textarea name="SEQ" cols="64" rows="5"></textarea>
                <p><i>Submit a file (&lt; 2MB) in <font color="#FF0000">FASTA</font> format directly from your local disk:</i><br>  		
                  <input name="SEQFILE" size=64 type="file">
                <p>
			<!--
                  <label>  	
			
		  <i>Amino acid type:</i><br>			
		  <span class="style2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		  <input name="lase_value" type="radio" value="kmodel" checked="checked">
		  <br>
	           <i>Amino acid type:</i><br>
		  <span class="style2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		  <input name="lase_value" type="radio" value="kmodel" checked="checked">K<br>
		  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		  <input name="lase_value" type="radio" value="rmodel" >R<br>
		  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		  <input name="lase_value" type="radio" value="tmodel" >T<br>
		  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		  <input name="lase_value" type="radio" value="pmodel" >P<br>
		  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		  </div>	
                  </label>
		-->
		</p>
                  <label>
                   <i>Select a Specificity Level:</i>      
            		<input name="Threshold" type="radio" value="High" checked="checked" />High (95%)
		            <input name="Threshold" type="radio" value="Medium" />Medium (90%)
		            <input name="Threshold" type="radio" value="Low" />Low (85%)
                  </label>
                </p>
                <input type="submit" value="Submit">
          		<input type="reset" value="Clear fields">
          		<input type="button" value="Example" 
		onclick="window.document.form1.SEQ.value='>GDIR1_MOUSE\nMAEQEPTAEQLAQIAAENEEDEHSVNYKPPAQKSIQEIQELDKDDESLRKYKEALLGRVAVSADPNVPNVIVTRLTLVCSTAPGPLELDLTGDLESFKKQSFVLKEGVEYRIKISFRVNREIVSGMKYIQHTYRKGVKIDKTDYMVGSYGPRAEEYEFLTPMEEAPKGMLARGSYNIKSRFTDDDKTDHLSWEWNLTIKKEWKD';">
          		</label>
             </form>
            <br>
			
			<li><a href="case1.php"><font color="#FE2EF7">Case Study 1</font></a> : ES1_MOUSE, ES1 protein homolog, mitochondrial.</li>
			<li><a href="case2.php"><font color="#FE2EF7">Case Study 2</font></a> : GDIR1_MOUSE, Rho GDP-dissociation inhibitor 1.</li>
            </td>
        </tr>
      </table></td>
    </tr>
</table>

<p>&nbsp;</p>

</div>


<?php
include("buttom.html");
?>

</div>
</body>
</html>

