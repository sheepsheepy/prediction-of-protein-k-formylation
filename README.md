# Prediction-of-protein-K-formylation-using-amino-acid-pair

Molecular biology / Amino Acid / Protein   
[Check our Website](http://140.138.150.147/~s1041431/)  

----- Using tools -----  
Data from PLMD (Protein Lysine Modifications Database) http://plmd.biocuckoo.org/  
CD-HIT http://weizhongli-lab.org/cd-hit/  
TwoSimpleLogo http://www.twosamplelogo.org/cgi-bin/tsl/tsl.cgi  
WebLogo http://weblogo.berkeley.edu/logo.cgi  
Weka https://www.cs.waikato.ac.nz/ml/weka/  

![Evaluation-Cross-Validation](information/Evaluation-Cross-Validation.png "Evaluation-Cross-Validation")
![IndependentTesting](information/IndependentTesting.png "IndependentTesting")

## Reference
[Prediction of protein N-formylation using the composition of k-spaced amino acid pairs](https://www.sciencedirect.com/science/article/pii/S0003269717303056?via%3Dihub)  
[笨蛋也可以用的 libsvm](http://www.cmlab.csie.ntu.edu.tw/~cyy/learning/tutorials/libsvm.pdf)  
[Check our Website](http://140.138.150.147/~s1041431/) 

Thank My partner for doing all the things with me for a whole year! Thank You!