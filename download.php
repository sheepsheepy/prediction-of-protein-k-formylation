﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="css/andreas01.css" media="screen" title="andreas01 (screen)" />
	<link rel="stylesheet" type="text/css" href="css/print.css" media="print" />

	<!--- Using css/CSS-download.css file-->
	<link rel="stylesheet" type="text/css" href="css/CSS-download.css"/>

	<?php include("pages/titleicon.html"); ?>
</head>
<body>
	<div id="wrap">
		<?php
			include("pages/top.html");
			include("pages/left.php");
			include("pages/right.php");
		?>
		<div id="content">
			<br>
			<h2 id="word">
				Data Set & Download
			</h2>
			<p id="word" align=justify>
				<font size="2">
				此研究從Protein Lysine Modifications Database (PLMD)中取得原始資料，再經過線上的網站CD-HIT來進行多次篩選，最後再進行比例的分配。<br>
				我們將CD-HIT的篩選結果與資料的比例分配列在下表，並附上比例1:1的資料數據提供查看。<br>
				</font>
			</p>
			<?php include("download/download-tablecdhit.php"); ?>
			<br>
			<?php include("download/download-tableproportion.php"); ?>
			<br>
			<?php include("download/download-tabledownload.php"); ?>
			<br>
			<h4 id="word">Reference:</h4>
			<?php include("download//download-reference.php"); ?>
		</div>
		<?php include("pages/buttom.html"); ?>
	</div>
</body>
</html>

