<?
//require('http://bidlab.life.nctu.edu.tw/~bryan/isblab_header.php');
?>
﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/andreas01.css" media="screen" title="andreas01 (screen)" />
<link rel="stylesheet" type="text/css" href="css/print.css" media="print" />
<title>SRPCat</title>
</head>

<style type="text/css">
<!--
.style1 {
	font-size: 15px;
	font-family: Arial, Helvetica, sans-serif;
}


.style22 {color: #FFFFFF; font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; }
.style24 {color: #FFFFFF; font-weight: bold; font-size: 12px; }
.style36 {font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #FFFFFF; font-weight: bold; }
.style37 {
	font-size: 14px;
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-weight: bold;
	color: #FFFFFF;
}


#myDiv {
		overflow: hidden;
		width: 920px;
		margin: 0 10px;
		/*background-color: red;*/
	}
	
#myDiv img {
		border: 0px solid;
		width: 450px;
		height: 500px;
	}


img {BORDER: 0px;}
-->
</style>

<body><div id="wrap">

<?php
//---------------------top.html--------------------------//
include("top.html");
?>

<?php
//---------------------menu.html--------------------------//
include("menu.html");
?>

<div id="content">
<br><br>
<h2>Result of Prediction</h2>
<hr>

<?

	$Sp_value = $_POST["Sp_value"];

	if($Sp_value == "90")
	{
		$Sp = 1; // 48.5
	}
	elseif($Sp_value == "85")
	{
		$Sp = 2; // 11
	}
	else{
		$Sp = 3;
	}

	
	$today = date("Ymd");      // 20040310
	$time = date("His");     // 17:16:17
	$day_time = $today.$time;
	if(!is_dir("tmp/".$today))	
		mkdir("tmp/".$today,0777);
	if(!is_dir("classify_tmp/".$today))
                mkdir("classify_tmp/".$today,0777);
	$fn = $today."/".$time;
	
	if ($_POST['SEQ']==null)
	{
    	if(copy($_FILES["SEQFILE"]["tmp_name"],"tmp/$fn.seq"))
    	{
        	$fi = fopen("tmp/$fn.seq","r");
        	while($temp = trim(fgets($fi,10000)))
        	{
            	if(substr($temp,0,1) == '>') $name = trim(substr($temp,1));  // ----get the input name 
            	else $seq = trim($temp);             // ----get the input sequence
        	}
    	}
    	else
    	{
        	echo "<div align=\"center\"><span class=\"style1 style15\"><font color=\"#EE9A00\">You Must Input Sequence or File.</font></span></div>";
        	die;
    	}
	}
	else
	{
    	if(substr($_POST['SEQ'],0,1) == '>')
    	{
        	$fp_seq=fopen("tmp/$fn.seq","w");
        	$input = str_replace(" ","",$_POST['SEQ']);
        	$input = str_replace("\t"," ",$input);
        	$input = strtoupper($input);
        	fputs($fp_seq,$input);
        	fclose($fp_seq);
        
        	$fi = fopen("tmp/$fn.seq","r");
        	while($temp = trim(fgets($fi,10000)))
        	{
            	if(substr($temp,0,1) == '>') $name = trim(substr($temp,1));  // ----get the input name 
            	else $seq = trim($temp);             // ----get the input sequence
        	}
        	fclose($fi);
    	}
    	else
    	{
        	echo "<div align=\"center\"><span class=\"style1 style15\"><font color=\"#EE9A00\">Your Sequence is not Fasta format.</font></span></div>";
        	die;
    	}
	}
	//start splicepred php program
	system("/home/splicepred/public_html/iprscan/bin/iprscan -cli -i ./tmp/$fn.seq -o ./tmp/$fn.dmout -format raw -goterms -iprlookup");
	system("cp tmp/$fn.dmout classify_tmp/$fn.dmout");
	system("php classification/ad_iprscan_out.php classify_tmp/$fn.dmout");
	system("php prediction/AAC.php tmp/$fn.seq"); // ****predict need adapt

	// Classification
	system("php classification/c1.php classify_tmp/$fn.dmseq 1 classify_tmp/$fn.1");
        system("php classification/c2.php classify_tmp/$fn.dmseq 1 classify_tmp/$fn.2");
        system("php classification/c3.php classify_tmp/$fn.dmseq 1 classify_tmp/$fn.3");
        system("php classification/c4.php classify_tmp/$fn.dmseq 1 classify_tmp/$fn.4");
		
	$train = "./all_train/Tr_all.txt";	//training data libsvm
	$model = "./all_model/Tr_all.txt.model";	//training data model
	$test = "./tmp/$fn.ac";
	$out = "./tmp/$fn.out";

	$train_c1 = "./all_train/Tr_c1.txt";    //training data libsvm
        $model_c1 = "./all_model/Tr_c1.txt.model";      //training data model
        $test_c1 = "./classify_tmp/$fn.1.dmlibsvm";
        $out_c1 = "./classify_tmp/$fn.1.out";

        $train_c2 = "./all_train/Tr_c2.txt";    //training data libsvm
        $model_c2 = "./all_model/Tr_c2.txt.model";      //training data model
        $test_c2 = "./classify_tmp/$fn.2.dmlibsvm";
        $out_c2 = "./classify_tmp/$fn.2.out";

        $train_c3 = "./all_train/Tr_c3.txt";    //training data libsvm
        $model_c3 = "./all_model/Tr_c3.txt.model";      //training data model
        $test_c3 = "./classify_tmp/$fn.3.dmlibsvm";
        $out_c3 = "./classify_tmp/$fn.3.out";

        $train_c4 = "./all_train/Tr_c4.txt";    //training data libsvm
        $model_c4 = "./all_model/Tr_c4.txt.model";      //training data model
        $test_c4 = "./classify_tmp/$fn.4.dmlibsvm";
        $out_c4 = "./classify_tmp/$fn.4.out";

	system("./prediction/PredQuickrbf $train $model $test $out");
        system("./prediction/PredQuickrbf $train_c1 $model_c1 $test_c1 $out_c1");
        system("./prediction/PredQuickrbf $train_c2 $model_c2 $test_c2 $out_c2");
        system("./prediction/PredQuickrbf $train_c3 $model_c3 $test_c3 $out_c3");
        system("./prediction/PredQuickrbf $train_c4 $model_c4 $test_c4 $out_c4");
	
	system("php prediction/iprscan_out.php ./tmp/$fn.dmout");
	system("php prediction/dm.php $fn ./tmp/$fn.dmimg");
	system("php classification/iprscan_out.php ./classify_tmp/$fn.dmout");
        system("php classification/dm.php $fn ./classify_tmp/$fn.dmimg");
?>
  </p>
  
    
  <?
  $fp_ac = fopen("./tmp/$fn.acstat","r");
  $fp_output = fopen("./tmp/$fn.out","r");
  $fp_output1 = fopen("./classify_tmp/$fn.1.out","r");
  $fp_output2 = fopen("./classify_tmp/$fn.2.out","r");
  $fp_output3 = fopen("./classify_tmp/$fn.3.out","r");
  $fp_output4 = fopen("./classify_tmp/$fn.4.out","r");
  $count = 0;
    
  while(!feof($fp_ac)){
	  $tmp = fgets($fp_ac);
	  $count++;
  }

  rewind($fp_ac);
  $fp_div_in = fopen("tmp/$fn.ac.jp", "r");

  for($i = 0; $i < $count - 1; $i++){
  $val = fgets($fp_div_in);
  $fp_div = fopen("tmp/$fn"."$i.ac.jp", "w");
  fwrite($fp_div,$val);
  fclose($fp_div);
  }
  
  for($i = 0; $i < $count - 1; $i++){
  system("php ./bartutex1.php tmp/$fn$i.ac.jp > tmp/$fn$i.png");
    
}
  //system("rm -f tmp/$fn*.ac.jp");
 
  for($i = 0; $i < $count - 1; $i++){
  $ac_out = fscanf($fp_ac, "%s\t%s\n");
  $quick_out = fscanf($fp_output, "%s\t%s\t%s\t%s\n");
  $pro1 = doubleval($quick_out[2]);
  $pro2 = doubleval($quick_out[3]);
  
  $c1_quick_out = fscanf($fp_output1, "%s\t%s\t%s\t%s\n");
  $c1_pro1 = doubleval($c1_quick_out[2]);
  $c1_pro2 = doubleval($c1_quick_out[3]);

  $c2_quick_out = fscanf($fp_output2, "%s\t%s\t%s\t%s\n");
  $c2_pro1 = doubleval($c2_quick_out[2]);
  $c2_pro2 = doubleval($c2_quick_out[3]);

  $c3_quick_out = fscanf($fp_output3, "%s\t%s\t%s\t%s\n");
  $c3_pro1 = doubleval($c3_quick_out[2]);
  $c3_pro2 = doubleval($c3_quick_out[3]);

  $c4_quick_out = fscanf($fp_output4, "%s\t%s\t%s\t%s\n");
  $c4_pro1 = doubleval($c4_quick_out[2]);
  $c4_pro2 = doubleval($c4_quick_out[3]);

  $seq_length = strlen($ac_out[1]); 
 
  
  
  $creat_seq_file = fopen('./tmp/'.$fn.'_'.$ac_out[0].'.sequence',"w");
  fwrite($creat_seq_file,$ac_out[1]);
  fclose($creat_seq_file);
  
  
  if($Sp == 1) $pro1 = $pro1 * 7;
  if($Sp == 2) $pro1 = $pro1 * 11;
  if($Sp == 3) $pro1 = $pro1 * 16;
  //$pro1 = $pro1 * 7;
 
  if($pro1 >= $pro2){
		echo "<br><br>";
		echo "<center>";
       
    echo "<table width=\"900\" border=\"0\">";
		echo "<tr>";
		echo "<td colspan=\"3\" background=\"back.gif\" style=\"color:#FFF\"><strong>&nbsp;&nbsp;&nbsp;Input File Information</strong></td>";
    echo "</tr>";
    echo "<tr>";
    echo "<td height=\"100\"  width=\"100\"><strong>Input ID</strong></td>";
    echo "<td height=\"100\"><strong>$ac_out[0]</strong></td>";
    echo "<td rowspan=\"4\"><br><center><img src='tmp/$fn$i.png'></center></td>";
    echo "</tr>";
    echo "<tr>";
    echo "<td height=\"30\" valign=\"top\"><strong>Length</strong></td>";
    echo "<td height=\"30\" valign=\"top\">$seq_length aa</td>";
    echo "</tr>";
    echo "<tr>";
    echo "<td height=\"30\" valign=\"top\"><strong>Classified</strong></td>";
    echo "<td height=\"30\" valign=\"top\">Splicing Related Protein</td>";
    echo "</tr>";
    echo "<tr>";
    echo "<td height=\"30\" valign=\"top\"><strong>Score</strong></td>";
    echo "<td height=\"30\" valign=\"top\">$pro1</td>";
    echo "</tr>";
    
    
    echo "</center>";
  }
  else{
  	echo "<center>";
       
    echo "<table width=\"900\" border=\"0\">";
		echo "<tr>";
		echo "<td colspan=\"3\" background=\"back.gif\" style=\"color:#FFF\"><strong>&nbsp;&nbsp;&nbsp;Input File Information</strong></td>";
    echo "</tr>";
    echo "<tr>";
    echo "<td height=\"100\" width=\"100\"><strong>Input ID:</strong></td>";
    echo "<td height=\"100\"><strong>$ac_out[0]</strong></td>";
    echo "<td rowspan=\"4\"><br><center><img src='tmp/$fn$i.png'></center></td>";
    echo "</tr>";
    echo "<tr>";
    echo "<td height=\"30\" valign=\"top\"><strong>Length</strong></td>";
    echo "<td height=\"30\" valign=\"top\">$seq_length aa</td>";
    echo "</tr>";
    echo "<tr>";
    echo "<td height=\"30\" valign=\"top\"><strong>Classified</strong></td>";
    echo "<td height=\"30\" valign=\"top\">Non-Splicing Related Protein</td>";
    echo "</tr>";
    echo "<tr>";
    echo "<td height=\"30\" valign=\"top\"><strong>Score</strong></td>";
    echo "<td height=\"30\" valign=\"top\">$pro1</td>";
    echo "</tr>";
    
    
    echo "</center>";

	}
  
  //count domain level
  
			$result = fopen('./tmp/'.$fn.'_'.$ac_out[0],"r");
			$ID = $ac_out[0];
			$sequence = $ac_out[1]; 
			$level = 0;
			$data="";
			
			while($data=fgets($result)) 
			{
				if($data != "\n")
        {
				$level++;
			  }
			}
			
			
	
	//show protein domain
			if($level > 0)
			{
				//echo $level;
				echo "<center>";
				echo "<table>";
				echo "<tr>";
				echo "<td width=896 background=\"back.gif\" style=\"color:#FFF\"><strong>&nbsp;&nbsp;&nbsp;Protein Domain</strong></td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td><iframe src='search_image_domain.php?ID=".$ID."&scale=8&width=".(strlen($sequence)*8+100)."&height=".($level*20+50)."&fn=".$fn."' width=896 height=".($level*20+100)." FRAMEBORDER=0 SCROLLING=yes></iframe></td>";
				echo "</tr>";
				echo "</table>";
				//echo "</center>";
				//echo "<br><br>";
				//echo "<hr>";
			}
  
    
  echo "</table>";

echo "<table width=\"900\" border=\"0\">";
                echo "<tr>";
                echo "<td colspan=\"5\" background=\"back.gif\" style=\"color:#FFF\"><strong>&nbsp;&nbsp;&nbsp;Category Identification</strong></td>";
                echo "</tr>";

                echo "<tr>";
                echo "<td width=\"150\" bgcolor=\"#FBB03B\"><strong><center>Categories</strong></center></td>";

if($level>0)
{
                if($c1_pro1 >= $c1_pro2)
                {
                        //echo "1 is pos;";
                        echo "<td height=\"30\" width=\"140\" background=\"pos.jpg\" style=\"color:#FFF\"><center><strong>snRNP</strong></center></td>";
                }
                else
                {
                        //echo "1 is neg;";
                        echo "<td height=\"30\" width=\"140\" background=\"back.gif\" style=\"color:#696969\"><center><strong>snRNP</strong></center></td>";
                }

                if($c2_pro1 >= $c2_pro2)
                {
                        //echo "2 is pos;";
                        echo "<td height=\"30\" width=\"150\" background=\"pos.jpg\" style=\"color:#FFF\"><center><strong>Splicing Factor</strong></center></td>";
                }
                else
                {
                        //echo "2 is neg;";
                        echo "<td height=\"30\" width=\"150\" background=\"back.gif\" style=\"color:#696969\"><center><strong>Splicing Factor</strong></center></td>";
                }

                if($c3_pro1 >= $c3_pro2)
                {
                        //echo "3 is pos;";
                        echo "<td height=\"30\" width=\"150\" background=\"pos.jpg\" style=\"color:#FFF\"><center><strong>Splicing Regulation Factor</strong></center></td>";
                }
                else

{
                        //echo "3 is neg;";
                        echo "<td height=\"30\" width=\"150\" background=\"back.gif\" style=\"color:#696969\"><center><strong>Splicing Regulation Factor</strong></center></td>";
                }

                if($c4_pro1 >= $c4_pro2)
                {
                        //echo "4 is pos;";
                        echo "<td height=\"30\" width=\"160\" background=\"pos.jpg\" style=\"color:#FFF\"><center><strong>Novel Spliceosome Protein</strong></center></td>";
                }
                else
                {
                        //echo "4 is neg;";
                        echo "<td height=\"30\" width=\"160\" background=\"back.gif\" style=\"color:#696969\"><center><strong>Novel Spliceosome Protein</strong></center></td>";
                }

                echo "</tr>";
                echo "<tr>";
                echo "<td bgcolor=\"#FBB03B\"><strong><center>Score</strong></center></td>";
                echo "<td height=\"30\" width=\"150\" bgcolor=\"#F1F1F1\"><center><strong>$c1_pro1</strong></center></td>";
                echo "<td height=\"30\" width=\"150\" bgcolor=\"#F1F1F1\"><center><strong>$c2_pro1</strong></center></td>";
                echo "<td height=\"30\" width=\"150\" bgcolor=\"#F1F1F1\"><center><strong>$c3_pro1</strong></center></td>";
                echo "<td height=\"30\" width=\"150\" bgcolor=\"#F1F1F1\"><center><strong>$c4_pro1</strong></center></td>";
                echo "</tr>";
}

else
{
echo "<td colspan=\"4\" height=\"30\" background=\"neg.jpg\" style=\"color:#FFF\"><center><strong>Sorry, there is no any functional domain !!</strong></center></td>";
echo "</tr>";
}

                echo "</table>";

                echo "</center>";

                echo "<br><br>";
                echo "<hr>";


}
  fclose($result);
  fclose($fp_ac);
  fclose($fp_output);
    
  ?>

<!--<div align=justify>

</div>-->


<?php
include("buttom.html");
?>

</div> <!--content over-->
</body>
</html>
