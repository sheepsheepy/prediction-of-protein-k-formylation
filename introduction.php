﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="css/andreas01.css" media="screen" title="andreas01 (screen)" />
		<link rel="stylesheet" type="text/css" href="css/print.css" media="print" />

		<!--- Using css/CSS-introduction.css file-->
		<link rel="stylesheet" type="text/css" href="css/CSS-introduction.css"/>

		<?php include("pages/titleicon.html"); ?>
	</head>

	<body>
		<div id="wrap">

			<?php
				include("pages/top.html");
				include("pages/left.php");
				include("pages/right.php");
			?>

			<div id="content">
				<br>
				<h1 id="word">Documentation</h1>
				<br>
				<h2 id="word">About Formylation</h2>
				<?php include("introduction/introduction-aboutformylation.php"); ?>
				<h2 id="word">Data Filtering</h2>
				<?php include("introduction/introduction-datafiltering.php"); ?>
				<h2 id="word">Feature Selection</h2>
				<?php include("introduction/introduction-featureselection.php"); ?>
				<h2 id="word">Model Training</h2>
				<?php include("introduction/introduction-modeltraining.php"); ?>
				<h2 id="word">Independent Test</h2>
				<?php include("introduction/introduction-independenttest.php"); ?>
				<h4 id="word">Reference:</h4>
				<?php include("introduction/introduction-reference.php"); ?>
			</div>

			<?php include("pages/buttom.html"); ?>

		</div>
	</body>
</html>

