<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/andreas01.css" media="screen" title="andreas01 (screen)" />
<link rel="stylesheet" type="text/css" href="css/print.css" media="print" />

	<!--- Using css/CSS-introduction.css file-->
	<link rel="stylesheet" type="text/css" href="css/CSS-case.css"/>

<?php include("pages/titleicon.html"); ?>

</head>

<body>
	<div id="wrap">
		<?php
			include("pages/top.html");
			include("pages/left.php");
			include("pages/right.php");
		?>
		<div id="content">
			<br><br>
			<h2 id="word">Training Model & Independent Test</h2>
			<br>
			<p id="word">
				<font size="2">
					● <a href="http://plmd.biocuckoo.org/">From Protein Lysine Modifications Database (PLMD)</a>: 
					<a href="http://plmd.biocuckoo.org/download.php">Formylation Download Site</a>
			    </font>
			</p>
			<p id="word">
				<font size="2">
					● UniprotKB/Species: 
					<a href="https://www.uniprot.org/proteomes/UP000000625">Escherichiacoli(strainK12)</a>
			    </font>
			</p>
			<p id="word">
				<font size="2">
					● PMIDs: 26840995
			    </font>
			</p>
			<br>
			<table id="formylation" align="center">
				<tbody>
					<tr>
						<th colspan="4">Formylation Site</th>
					</tr>
					<tr>
						<th colspan="2">Training Model</th>
						<th colspan="2">Independent Test</th>
					</tr>
					<tr>
						<td>
							<a href="case/case-tablemodel.php">Check Training Model Data</a>
						</td>
						<td>
							<a href="predict-training.php">Predict Training Model</a>
						</td>
						<td>
							<a href="case/case-tabletesting.php">Check Independent Test Data</a>
						</td>
						<td>
							<a href="predict-testing.php">Predict Testing Model</a>
						</td>
					</tr>
				</tbody>
			</table>
			<br>
		</div>
		<?php include("pages/buttom.html"); ?>
	</div>
</body>
</html>

