<?php
$frag = $argv[1];
$seq=array();
$count = -1;
$central_aa = "K";

$file = fopen($frag,"r");
$out = fopen("{$frag}.aac.libsvm","w"); //output akhir dari program
$jp = fopen("{$frag}.ac.jp","w"); //output akhir dari program

$class=1;
$count=1;

while($line=fgets($file))
{
        $aac=array('A'=>0, 'R'=>0, 'N'=>0, 'D'=>0, 'C'=>0, 'Q'=>0, 'E'=>0, 'G'=>0, 'H'=>0, 'I'=>0, 'L'=>0, 'K'=>0, 'M'=>0, 'F'=>0, 'P'=>0, 'S'=>0, 'T'=>0, 'W'=>0, 'Y'=>0, 'V'=>0, 'X'=>0);
        $id=strtok($line, "\t");
	$pos=strtok("\t");
        //echo $id."\n";
        $sequence=strtok("\t");
        //echo $sequence."\n";
        $sequence=trim($sequence);
        $length=strlen($sequence);
        for($i=0;$i<$length;$i++)
        {
                $aa=$sequence[$i]; //amino acid position = $i+1
                switch ($aa)
                {
                        case ($aa=='X' || $aa== 'B' || $aa=='U' || $aa=='O' || $aa=='Z' || $aa=='J' || $aa=='-'):
                        $aac['X']++;
                        break;
                        default:
                                        $aac[$aa]++;
                                                                        //break;
                }//switch
        }//for

        $index=1;

        fwrite($out, "$class ");
	//fwrite($jp, "$count;");

        foreach($aac as $key=>$val)
        {
		if($key==$central_aa)
			$val = $val-1;		

                $dummy=$val/($length-1);
                //echo "$dummy\n";
                $value=round($dummy,4);
                //echo "$value\n";
                fwrite($out, "{$index}:{$value} ");
                fwrite($jp, "$value ");
                $index++;
        }//foreach
        fwrite($out, "\n");
        fwrite($jp, "\n");
	$count++;
}//while
fclose($out);
fclose($jp);
fclose($file);

//unlink($output_stat);
?>

