<table id="testingdata" align="center">
    <tbody>
		<tr>
		<th colspan="13">1:1 Amino Acid Composition LibSVM with Testing Data</th>
        </tr>
		<tr>
		<th colspan="1">TP</th>
		<th colspan="1">FN</th>
        <th colspan="1">FP</th>
		<th colspan="1">TN</th>
		<th colspan="1">SN</th>
		<th colspan="1">SP</th>
        <th colspan="1">ACC</th>
		<th colspan="1">MCC</th>
		<th colspan="1">AUC</th>
        </tr>	
		<tr>
		<td colspan="1">45</td>
		<td colspan="1">8</td>
        <td colspan="1">13</td>
		<td colspan="1">42</td>
		<td colspan="1">84.91%</td>
		<td colspan="1">76.36%</td>
        <td colspan="1">81%</td>
		<td colspan="1">0.61</td>
		<td colspan="1">0.85</td>
        </tr>
    </tbody>
</table>