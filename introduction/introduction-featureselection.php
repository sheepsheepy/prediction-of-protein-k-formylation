<h4 id="word">Amino Acid Composition（AAC）</h4>
<p id="word" align="justify">
	<font size="2">
		此分析法為針對每個胺基酸個體進行分析，不進行任何組合與配對，也是最直觀的分析法。
		<br><br>
		(1)	胺基酸位置點比例分析
		<br><br>
		我們使用網站WebLogo分析每個位置的胺基酸比例。此網站統計每個位置點的胺基酸出現次數進行計算，並以此比例設定英文字母的高低後由多至少從上往下排列每個直行。
		<br>
	</font>
</p>
<br>
<div id="weblogo" align="center">
	<img src="images/postive-plot.png"/>
</div>
<div id="weblogo" align="center">
	<img src="images/negative-100-plot.png"/>
	<img src="images/negative-200-plot.png"/>
	<img src="images/negative-300-plot.png"/>
	<img src="images/negative-400-plot.png"/>
</div>
<br>
<p id="word" align="justify">
	<font size="2">	
		網站Two Sample Logo把Positive Data與Negative Data的每個位置點數據進行相減比較之後算出差距值。
	</font>
</p>
<br>
<div id="weblogo" align="center">
	<img src="images/pos1&neg1.png"/>
	<img src="images/pos1&neg2.png"/>
	<img src="images/pos1&neg3.png"/>
	<img src="images/pos1&neg4.png"/>
</div>
<br>
<p id="word" align="justify">
	<font size="2">
		(2)	胺基酸比重分析
		<br><br>
		把整筆資料的胺基酸比重統計後繪製長條圖。
	</font>
</p>
<div align="center">
	<img width="500px" src="images/longfig.png"/>
</div>
<br>
<h4 id="word">Amino Acid Pair Composition（AAPC）</h4>
<p id="word" align="justify">
	<font size="2">
		這個分類法是將胺基酸兩個為一組，用以看出組合的差別。
		<br><br>
		我們將21x21種組合統計過後，畫出比較圖。
	</font>
</p>
<br>
<div id="bluered" align="center">
	<img src="images/positive1negative1.jpg"/>
	<img src="images/positive1negative2.jpg"/>
	<img src="images/positive1negative3.jpg"/>
	<img src="images/positive1negative4.jpg"/>
</div>
<br>