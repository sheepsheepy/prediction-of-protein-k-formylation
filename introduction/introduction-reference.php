<p id="word">
	WebLogo: A sequence logo generator
	<br>
	<a href="http://weblogo.berkeley.edu/logo.cgi">http://weblogo.berkeley.edu/logo.cgi</a>
	<br>
	<a href="http://www.twosamplelogo.org/cgi-bin/tsl/tsl.cgi">http://www.twosamplelogo.org/cgi-bin/tsl/tsl.cgi</a>
	<br>
	Glycosylation and other PTMs alterations in neurodegenerative diseases: Current status and future role in neurotrauma.
	<br>
	<a href ="https://www.ncbi.nlm.nih.gov/pubmed/26957254">https://www.ncbi.nlm.nih.gov/pubmed/26957254</a>
	<br>
	Do post-translational beta cell protein modifications trigger type 1 diabetes?
	<br>
	<a href ="https://www.ncbi.nlm.nih.gov/pubmed/24048671">https://www.ncbi.nlm.nih.gov/pubmed/24048671</a>
	<br>
	Post‐translational modifications of the cardiac proteome in diabetes and heart failure. 
	<br>
	<a href ="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4698356/">https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4698356/</a>
	<br>
	ActiveDriverDB: human disease mutations and genome variation in post-translational modification sites of proteins.
	<br>
	<a href ="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5753267/">https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5753267/</a>
	<br>
	Protein Modifications as Potential Biomarkers in Breast Cancer. Biomark Insights.
	<br>
	<a href ="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2805424/">https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2805424/</a>
	<br>
	Nε-Formylation of lysine is a widespread post-translational modification of nuclear proteins occurring at residues involved in regulation of chromatin function.
	<br>
	<a href ="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2241850/">https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2241850/</a>
	<br>
	Mutations in MTFMT underlie a human disorder of formylation causing impaired mitochondrial translation.
	<br>
	<a href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3486727/">https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3486727/</a>
	<br>
	Lysine post-translational modifications of collagen.
	<br>
	<a href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3499978/">https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3499978/</a>
	<br>
	SIRT2 and lysine fatty acylation regulate the transforming activity of K-Ras4a.
	<br>
	<a href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5745086/">https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5745086/</a>
	<br>
	Emerging role of post-translational modifications in chronic kidney disease and cardiovascular disease. Nephrology 
	<br>
	<a href="https://academic.oup.com/ndt/article/30/11/1814/2459919">https://academic.oup.com/ndt/article/30/11/1814/2459919</a>
	<br>
	Increased Post-Translational Lysine Acetylation of Myelin Basic Protein Is Associated with Peak Neurological Disability in a Mouse Experimental Autoimmune Encephalomyelitis Model of Multiple Sclerosis.
	<br>
	<a href="https://www.ncbi.nlm.nih.gov/pubmed/29111742">https://www.ncbi.nlm.nih.gov/pubmed/29111742</a>
	<br>
	N-formylation of lysine in histone proteins as a secondary modification arising from oxidative DNA damage. 
	<br>
	<a href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1765477/">https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1765477/</a>
	<br>
</p>