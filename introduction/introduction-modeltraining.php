<p id="word" align="justify">
	<font size="2">
		我們將訓練的結果列在下方表格，並劃出ROC曲線跟算出AUC。
	</font>
</p>
<br>
<?php include("introduction/introduction-tableaacaapc.php"); ?>
<br>
<h2 id="word">ROC:</h2>
<br>
<div id="ROC" align="center">
	<img src="images/ROCAAC.png"/>
	<img src="images/ROCAAPC.png"/>
	<img src="images/ROCAACAAPC.png"/>
</div>
<br>