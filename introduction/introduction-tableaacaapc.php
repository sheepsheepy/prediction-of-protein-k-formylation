<table id="aac" align="center">
    <tbody>
		<tr>
		<th colspan="13">Amino Acid Composition（AAC）Fold = 5</th>
        </tr>
		<tr>
		<th colspan="1">Proportion</th>
		<th colspan="3">Algorithm</th>
		<th colspan="1">TP</th>
		<th colspan="1">FN</th>
        <th colspan="1">FP</th>
		<th colspan="1">TN</th>
		<th colspan="1">SN</th>
		<th colspan="1">SP</th>
        <th colspan="1">ACC</th>
		<th colspan="1">MCC</th>
		<th colspan="1">AUC</th>
        </tr>
		<tr>
		<td colspan="1">1:1</td>
		<td colspan="3">RandomForest</td>
		<td colspan="1">68</td>
		<td colspan="1">32</td>
        <td colspan="1">36</td>
		<td colspan="1">64</td>
		<td colspan="1">68%</td>
		<td colspan="1">64%</td>
        <td colspan="1">66%</td>
		<td colspan="1">0.32</td>
		<td colspan="1">0.66</td>
        </tr>
		<tr>
		<td colspan="1">1:1</td>
		<td colspan="3">LibSVM</td>
		<td colspan="1">75</td>
		<td colspan="1">25</td>
        <td colspan="1">29</td>
		<td colspan="1">72</td>
		<td colspan="1">75%</td>
		<td colspan="1">71%</td>
        <td colspan="1">73%</td>
		<td colspan="1">0.46</td>
		<td colspan="1">0.62</td>
        </tr>
		<tr>
		<td colspan="1">1:1</td>
		<td colspan="3" >IBK</td>
		<td colspan="1">72</td>
		<td colspan="1">28</td>
        <td colspan="1">53</td>
		<td colspan="1">47</td>
		<td colspan="1">72%</td>
		<td colspan="1">47%</td>
        <td colspan="1">60%</td>
		<td colspan="1">0.2</td>
		<td colspan="1">0.39</td>
        </tr>
		<tr>
		<td colspan="1">1:1</td>
		<td colspan="3">J48</td>
		<td colspan="1">63</td>
		<td colspan="1">37</td>
        <td colspan="1">43</td>
		<td colspan="1">57</td>
		<td colspan="1">63%</td>
		<td colspan="1">57%</td>
        <td colspan="1">60%</td>
		<td colspan="1">0.2</td>
		<td colspan="1">0.37</td>
        </tr>
    </tbody>
</table>

<br>

<table id="aapc" align="center">
    <tbody>
		<tr>
		<th colspan="13">Amino Acid Pair Composition（AAPC）Fold = 5</th>
        </tr>
		<tr>
		<th colspan="1">Proportion</th>
		<th colspan="3">Algorithm</th>
		<th colspan="1">TP</th>
		<th colspan="1">FN</th>
        <th colspan="1">FP</th>
		<th colspan="1">TN</th>
		<th colspan="1">SN</th>
		<th colspan="1">SP</th>
        <th colspan="1">ACC</th>
		<th colspan="1">MCC</th>
		<th colspan="1">AUC</th>
        </tr>
		<tr>
		<td colspan="1">1:2</td>
		<td colspan="3">RandomForest</td>
		<td colspan="1">56</td>
		<td colspan="1">44</td>
        <td colspan="1">91</td>
		<td colspan="1">109</td>
		<td colspan="1">56%</td>
		<td colspan="1">54.4%</td>
        <td colspan="1">55%</td>
		<td colspan="1">0.1</td>
		<td colspan="1">0.57</td>
        </tr>
		<tr>
		<td colspan="1">1:1</td>
		<td colspan="3">LibSVM</td>
		<td colspan="1">58</td>
		<td colspan="1">42</td>
        <td colspan="1">79</td>
		<td colspan="1">121</td>
		<td colspan="1">58%</td>
		<td colspan="1">60.5%</td>
        <td colspan="1">60%</td>
		<td colspan="1">0.18</td>
		<td colspan="1">0.59</td>
        </tr>
		<tr>
		<td colspan="1">1:1</td>
		<td colspan="3">IBK</td>
		<td colspan="1">56</td>
		<td colspan="1">44</td>
        <td colspan="1">51</td>
		<td colspan="1">49</td>
		<td colspan="1">56%</td>
		<td colspan="1">49%</td>
        <td colspan="1">53%</td>
		<td colspan="1">0.05</td>
		<td colspan="1">0.38</td>
        </tr>
		<tr>
		<td colspan="1">1:1</td>
		<td colspan="3">J48</td>
		<td colspan="1">56</td>
		<td colspan="1">44</td>
        <td colspan="1">90</td>
		<td colspan="1">110</td>
		<td colspan="1">56%</td>
		<td colspan="1">55%</td>
        <td colspan="1">55%</td>
		<td colspan="1">0.1</td>
		<td colspan="1">0.13</td>
        </tr>
    </tbody>
</table>

<br>

<table id="aacaapc" align="center">
    <tbody>
		<tr>
		<th colspan="13">Amino Acid Composition + Amino Acid Pair Composition（AAC + AAPC）Fold = 5</th>
        </tr>
		<tr>
		<th colspan="1">Proportion</th>
		<th colspan="3">Algorithm</th>
		<th colspan="1">TP</th>
		<th colspan="1">FN</th>
        <th colspan="1">FP</th>
		<th colspan="1">TN</th>
		<th colspan="1">SN</th>
		<th colspan="1">SP</th>
        <th colspan="1">ACC</th>
		<th colspan="1">MCC</th>
		<th colspan="1">AUC</th>
        </tr>
		
		<tr>
		<td colspan="1">1:1</td>
		<td colspan="3">RandomForest</td>
		<td colspan="1">64</td>
		<td colspan="1">36</td>
        <td colspan="1">45</td>
		<td colspan="1">55</td>
		<td colspan="1">64%</td>
		<td colspan="1">55%</td>
        <td colspan="1">60%</td>
		<td colspan="1">0.19</td>
		<td colspan="1">0.46</td>
        </tr>
		<tr>
		<td colspan="1">1:2</td>
		<td colspan="3">RandomForest</td>
		<td colspan="1">66</td>
		<td colspan="1">34</td>
        <td colspan="1">85</td>
		<td colspan="1">115</td>
		<td colspan="1">66%</td>
		<td colspan="1">57.5%</td>
        <td colspan="1">60%</td>
		<td colspan="1">0.22</td>
		<td colspan="1">0.61</td>
        </tr>
		<tr>
		<td colspan="1">1:2</td>
		<td colspan="3">LibSVM</td>
		<td colspan="1">68</td>
		<td colspan="1">32</td>
        <td colspan="1">70</td>
		<td colspan="1">130</td>
		<td colspan="1">68%</td>
		<td colspan="1">65%</td>
        <td colspan="1">66%</td>
		<td colspan="1">0.31</td>
		<td colspan="1">0.71</td>
        </tr>
		<tr>
		<td colspan="1">1:2</td>
		<td colspan="3">IBK</td>
		<td colspan="1">59</td>
		<td colspan="1">41</td>
        <td colspan="1">89</td>
		<td colspan="1">111</td>
		<td colspan="1">59%</td>
		<td colspan="1">55.5%</td>
        <td colspan="1">57%</td>
		<td colspan="1">0.14</td>
		<td colspan="1">0.41</td>
        </tr>
		<tr>
		<td colspan="1">1:2</td>
		<td colspan="3">J48</td>
		<td colspan="1">58</td>
		<td colspan="1">42</td>
        <td colspan="1">89</td>
		<td colspan="1">111</td>
		<td colspan="1">58%</td>
		<td colspan="1">55.5%</td>
        <td colspan="1">56%</td>
		<td colspan="1">0.13</td>
		<td colspan="1">0.15</td>
        </tr>
    </tbody>
</table>